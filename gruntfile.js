module.exports = function (grunt) {
  grunt.initConfig({
    nodemon: {
      dev: {
        script: './server/server.js'
      }
    },
    browserify: {
      dist: {
        options: {
          transform: [
            ["babelify", {
              presets: ["react", "es2015"]
            }]
          ]
        },
        files: {
          "./build/bundle.js": ["./jsx/test-entry.jsx"]
        },
        watch: {
            files: ["./jsx/*.js"],
            tasks: ["browserify"]
          }
      }
    }
  });


  grunt.loadNpmTasks("grunt-browserify");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks('grunt-nodemon');

  grunt.registerTask("default", ["browserify", "nodemon"]);
  grunt.registerTask("build", ["browserify"]);
  };
