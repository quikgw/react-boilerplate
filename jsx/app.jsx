var React = require('react');
var ReactDOM = require('react-dom');
var ListContainer = require('./components/list-container.jsx');

var app = React.createClass({
  propTypes: {
    data: React.PropTypes.object.isRequired
  },

  render: function () {
    return (
      <div id='App' className="asdf">
        App test

        <ListContainer list={this.props.data.list} />
      </div>
    )
  }
});

module.exports = app;






