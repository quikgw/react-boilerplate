var React = require('react');
var ReactDOM = require('react-dom');
var Reflux = require('reflux');
var _ = require('lodash');
var ListItem = require('./list-item.jsx');
var TestAction = require('../actions/test-actions.js');
var TestStore = require('../stores/test-store');

var ListContainer = React.createClass({
  mixins: [Reflux.connect(TestStore,"book")],

  propTypes: {
    list: React.PropTypes.array.isRequired
  },

  getInitialState: function() {
    return {
      book: {
        selected: 0
      },
      previousSelected: "",
      selected: ""
    }
  },

  renderList: function() {
    var list = [];

    _(this.props.list).forEach(function(value, key) {
      var selected = value.id == this.state.selected ? true: false;
      if (this.state.previousSelected != value.id) {
        list.push(
          <ListItem key={key} data={value} selection={this.selectItem} selected={selected}/>
        );
      }
    }.bind(this));

    return list;
  },

  selectItem: function(id) {
    TestAction.nameUpdate(id);
    this.setState({previousSelected: this.state.selected});
  },

  render: function () {
    var selectedIndex = _.findIndex(this.props.list, function(o) {
      return o.id == this.state.book.selected;
    }.bind(this));

    return (
      <div>
        <div>List-Container</div>
        <div>Selected: {selectedIndex > -1 ? this.props.list[selectedIndex].name : ''}</div>


        <ul className="list-container">
          {this.renderList()}
        </ul>
      </div>
    )
  }
});

module.exports = ListContainer;
