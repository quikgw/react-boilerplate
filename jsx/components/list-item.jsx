var React = require('react');
var ReactDOM = require('react-dom');

var ListItem = React.createClass({
  propTypes: {
    data: React.PropTypes.object.isRequired,
    selection: React.PropTypes.func.isRequired,
    selected: React.PropTypes.bool.isRequired
  },

  getInitialState: function() {
    return null;
  },

  componentWillUnmount: function() {
    console.log('I unmounted ' + this.props.data.id);
  },

  onClick: function(e) {
    this.props.selection(this.props.data.id);
  },

  render: function () {
    var listClass= this.props.selected ? "list-item selected" : "list-item";

    return (
      <li className={listClass} onClick={this.onClick}>
        <div>Name: {this.props.data.name}</div>
        <div>Age: {this.props.data.age}</div>
        <div>Id: {this.props.data.id}</div>
      </li>
    )
  }
});

module.exports = ListItem;
