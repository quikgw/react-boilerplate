'use strict';

var Reflux = require('reflux');
var TestActions = require('../actions/test-actions');

module.exports = Reflux.createStore({
  displayName: 'TestStore',

  data: {
    "selected" : ''
  },

  init: function() {
    this.listenTo(TestActions.nameUpdate, this.onNameUpdate);
  },

  onNameUpdate: function(selected) {
    this.data.selected = selected;

    this.trigger(this.data);
  }
});
