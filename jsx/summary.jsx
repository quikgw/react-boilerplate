var React = require('react');
var ReactDOM = require('react-dom');
var Reflux = require('reflux');

var TestStore = require('./stores/test-store');

var summary = React.createClass({
  mixins: [Reflux.connect(TestStore,"book")],

  propTypes: {},

  getInitialState: function() {
    return ({
      book : {
        selected: 0
      }
    })
  },

  render: function () {
    return (
      <div id='Summary'>
        Summary: {this.state.book.selected}
      </div>
    )
  }
});

module.exports = summary;
