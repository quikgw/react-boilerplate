var React = require('react');
var ReactDOM = require('react-dom');
var App = require('./app.jsx');
var Summary = require('./summary.jsx');


var data = document.getElementById('data');
var dataContent = JSON.parse(data.innerHTML);

ReactDOM.render(<App data={dataContent} />, document.getElementById('main-container'));
ReactDOM.render(<Summary />, document.getElementById('side-container'));
